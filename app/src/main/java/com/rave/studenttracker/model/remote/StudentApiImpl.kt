package com.rave.studenttracker.model.remote

import com.rave.studenttracker.model.dto.StudentDTO
import kotlinx.coroutines.delay
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json

const val DELAY_TIME = 3000L

internal class StudentApiImpl(fakeJsonString: String) : StudentApi {

    private val fakeStudents: List<StudentDTO> by lazy { Json.decodeFromString(fakeJsonString) }

    override suspend fun fetchStudentList(): List<StudentDTO> {
        delay(DELAY_TIME)
        return fakeStudents
    }
}
