package com.rave.studenttracker.model.entity

/**
 * Student is a class containing information of a single student.
 *
 * @property avatar is the student's profile picture
 * @property email is the student's email
 * @property firstName is the student's first name
 * @property id is the student's unique ID
 * @property lastName is the student's last name
 * @property university is the university the student is attending
 * @constructor Creates an Student based on the parameters
 */
data class Student(
    val avatar: String,
    val email: String,
    val firstName: String,
    val id: Int,
    val lastName: String,
    val university: String
)
