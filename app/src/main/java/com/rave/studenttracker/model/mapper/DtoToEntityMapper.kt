package com.rave.studenttracker.model.mapper

/**
 * Dto to entity mapper takes a Student Data transfer object and converts it to a Student.
 *
 * @param DTO the Data Transfer object taken from the JSON
 * @param ENTITY the resulting Student
 * @constructor Create empty Dto to entity mapper
 */
interface DtoToEntityMapper<in DTO, out ENTITY> {
    /**
     * Invoke allows a Student DTO to be mapped to a Student by invoking the [DtoToEntityMapper] .
     *
     * @param dto the DTO to be transferred
     * @return the resulting Student
     */
    operator fun invoke(dto: DTO): ENTITY
}
