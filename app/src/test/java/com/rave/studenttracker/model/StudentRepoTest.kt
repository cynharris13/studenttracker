package com.rave.studenttracker.model

import com.rave.studenttracker.model.dto.StudentDTO
import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import com.rave.studenttracker.model.utilTest.CoroutinesTestExtension
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.RegisterExtension

internal class StudentRepoTest {
    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()

    private val api = mockk<StudentApi>()
    private val mapper = StudentMapper()
    private val repo = StudentRepo(
        studentApi = api,
        studentMapper = mapper,
        dispatcher = coroutinesTestExtension.dispatcher
    )

    @Test
    @DisplayName("Test to get the student list")
    fun testGetStudentList() = runTest(coroutinesTestExtension.dispatcher) {
        // given
        val studentDTOs = listOf(
            StudentDTO(
                avatar = "",
                email = "",
                firstName = "",
                lastName = "",
                university = "",
                id = 0
            )
        )
        coEvery { api.fetchStudentList() } coAnswers { studentDTOs }
        // when
        val studentList = repo.getStudentList()
        // then
        val students = studentDTOs.map { dto -> mapper(dto) }
        Assertions.assertEquals(students, studentList)
    }
}
