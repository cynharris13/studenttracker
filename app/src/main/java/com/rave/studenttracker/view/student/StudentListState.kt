package com.rave.studenttracker.view.student

import com.rave.studenttracker.model.entity.Student

/**
 * Student list state keeps track of whether the coroutines to get the [Student] list is still running,
 * and if not, what the list of students are.
 *
 * @property isLoading is true while the process of retrieving students is ongoing
 * @property students the resulting list of [Student]
 * @constructor Create empty Student list state
 */
data class StudentListState(
    val isLoading: Boolean = false,
    val students: List<Student> = emptyList()
)
