package com.rave.studenttracker.viewmodel

import com.rave.studenttracker.model.StudentRepo
import com.rave.studenttracker.model.entity.Student
import com.rave.studenttracker.model.utilTest.CoroutinesTestExtension
import com.rave.studenttracker.model.utilTest.InstantTaskExecutor
import com.rave.studenttracker.view.student.StudentListState
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.junit.jupiter.api.extension.RegisterExtension

@OptIn(ExperimentalCoroutinesApi::class)
@ExtendWith(InstantTaskExecutor::class)
internal class StudentListViewModelTest {
    @RegisterExtension
    private val coroutinesTestExtension = CoroutinesTestExtension()
    private val repo = mockk<StudentRepo>()

    private val students = listOf(
        Student(
            avatar = "",
            email = "",
            firstName = "",
            lastName = "",
            university = "",
            id = 0
        )
    )

    @Test
    @DisplayName("Tests state starts off loading and then new state has students")
    fun testLoadingThenFetchOnCreation() = runTest(coroutinesTestExtension.dispatcher) {
        // given
        coEvery { repo.getStudentList() } coAnswers { students }

        val stateUpdates = mutableListOf<StudentListState>()
        // when
        val studentListViewModel = StudentListViewModel(repo)
        val job = launch { studentListViewModel.studentListState.toList(stateUpdates) }
        studentListViewModel.fetchStudents()

        // then
        val (initState, loadingState, successState) = stateUpdates
        Assertions.assertFalse(initState.isLoading)
        Assertions.assertTrue(initState.students.isEmpty())

        Assertions.assertTrue(loadingState.isLoading)
        Assertions.assertTrue(loadingState.students.isEmpty())

        Assertions.assertTrue(successState.students.isNotEmpty())
        Assertions.assertFalse(successState.isLoading)
        job.cancel()
    }
}
