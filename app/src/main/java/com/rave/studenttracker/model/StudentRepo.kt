package com.rave.studenttracker.model

import com.rave.studenttracker.model.entity.Student
import com.rave.studenttracker.model.mapper.student.StudentMapper
import com.rave.studenttracker.model.remote.StudentApi
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Student repo accesses the [StudentApi], takes the DTO objects, maps them with [studentMapper],
 * and returns the [Student] objects.
 *
 * @property studentApi the remote database with the student DTOs
 * @property studentMapper the tool to map Student DTOs into [Student]
 * @property dispatcher the thread this coroutine will be run on
 * @constructor Create empty Student repo
 */
class StudentRepo(
    private val studentApi: StudentApi,
    private val studentMapper: StudentMapper,
    private val dispatcher: CoroutineDispatcher = Dispatchers.IO
) {
    /**
     * Get student list returns a list students after retrieving them in data transfer objects.
     *
     * @return the list of [Student]
     */
    suspend fun getStudentList(): List<Student> {
        val studentDTOList = withContext(dispatcher) { studentApi.fetchStudentList() }
        return studentDTOList.map { studentMapper(it) }
    }
}
