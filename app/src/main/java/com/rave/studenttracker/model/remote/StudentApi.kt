package com.rave.studenttracker.model.remote

import com.rave.studenttracker.model.dto.StudentDTO

/**
 * Student api is the interface for getting [StudentDTO] from the JSON.
 *
 * @constructor Create empty Student api
 */
interface StudentApi {
    /**
     * Fetch student list is the method to be impemented to fetch students from the JSON.
     *
     * @return a list of [StudentDTO]
     */
    suspend fun fetchStudentList(): List<StudentDTO>
}
