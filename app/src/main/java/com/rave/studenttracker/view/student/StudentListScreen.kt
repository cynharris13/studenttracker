package com.rave.studenttracker.view.student

import androidx.compose.foundation.background
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import coil.compose.AsyncImage
import com.rave.studenttracker.model.entity.Student
import kotlin.random.Random

const val MAX_VALUE = 256
const val BIT_FACTOR = 255

@Composable
fun StudentListScreen(students: List<Student>) {
    LazyColumn(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier.background(Color.Black)
    ) {
        items(
            items = students,
            key = { student: Student -> student.id }
        ) { student: Student ->
            StudentCard(student = student)
        }
    }
}

@Composable
fun StudentCard(student: Student) {
    val randomColor = Color(
        Random.nextInt(0, MAX_VALUE),
        Random.nextInt(0, MAX_VALUE),
        Random.nextInt(0, MAX_VALUE)
    )
    val textColor = Color(
        (BIT_FACTOR * (1 - randomColor.red)).toInt(),
        (BIT_FACTOR * (1 - randomColor.green)).toInt(),
        (BIT_FACTOR * (1 - randomColor.blue)).toInt()
    )
    Card(colors = CardDefaults.cardColors(containerColor = randomColor, contentColor = textColor)) {
        Text(text = student.firstName + " " + student.lastName, textAlign = TextAlign.Center)
        AsyncImage(model = student.avatar, contentDescription = null)
        Text(text = "Attends ${student.university}", textAlign = TextAlign.Center)
        Text(text = student.email, textAlign = TextAlign.Center)
    }
}
